package net.coconauts.waterrings;

import android.content.Context;
import android.media.MediaPlayer;
import android.util.Log;

public  class Sounds {

	static MediaPlayer soundAirPump;
	static MediaPlayer[] soundBubble;
	static int maxSoundBubble = 4;

	public static void initSound(Context c) {

		Log.d("sound", "inicializando sound");
		soundAirPump = MediaPlayer.create(c, R.raw.air_pumping);

		soundBubble = new MediaPlayer[maxSoundBubble];
		soundBubble[0] = MediaPlayer.create(c, R.raw.bubble0);
		soundBubble[1] = MediaPlayer.create(c, R.raw.bubble1);
		soundBubble[2] = MediaPlayer.create(c, R.raw.bubble2);
		soundBubble[3] = MediaPlayer.create(c, R.raw.bubble3);
		Log.d("sound", "inicializado soundBubble " + soundBubble[0]);
	}
	public static void playSoundAirPump(Context c) {

		if (Prefs.isSoundActive(c) ) {
			if (soundAirPump == null) {
				Log.w("sound", "Sound not initialized " + soundBubble);
			} else {
				Log.w("sound", "Sound initialized " + soundBubble);
				soundAirPump.start();
			}
		}

	}
	public static void playSoundBubble(Context c) {

		if (Prefs.isSoundActive(c) ) {
			if (soundBubble == null) {
				Log.w("sound", "Sound not initialized " + soundBubble);
			} else {
				Log.w("sound", "Sound initialized " + soundBubble);

				int random = (int) (Math.random() * 4);
				Log.d("sound", "sound random " + random);
				Log.d("sound", "soundBubble " + soundBubble[random]);

				if (soundBubble[random] == null) {
					Log.d("sound", "sound random is null ");
				} else {
					soundBubble[random].start();
				}
			}
		}

	}
}