package net.coconauts.waterrings;

import java.util.ArrayList;
import java.util.List;

import net.coconauts.waterrings.Circle;
import net.coconauts.waterrings.R;

import android.content.Context;
import android.graphics.*;
import android.graphics.Path.Direction;
import android.os.Handler;
import android.os.Message;

import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class GameView extends View {

	private RefreshHandler redraw = new RefreshHandler();
	private float powerLeft = 0;
	private float powerRight = 0;
	private boolean pressedLeft = false;
	private boolean pressedRight = false;
	//private Sprite spriteTest;
	private Circle circleLeft;
	private Circle circleRight;
	private List<Circle> listBubbleLeft;
	private List<Circle> listBubbleRight;
	private Sprite[] arrayRings;
	private int maxRings;
	private Sprite background;
	private Sprite stickLeft;
	private Sprite stickRight;
	private Path pathLeft;
	private Path pathRight;
	private boolean isComplete;
	private boolean stickUp;
	private boolean limitedAir;
	
	class RefreshHandler extends Handler {

		@Override
		public void handleMessage(Message msg) {
			// Log.d("RefreshHandler","handle");

			GameView.this.update();
			GameView.this.invalidate();
			// SnakeView.this.invalidate();
		}

		public void sleep(long delayMillis) {

			this.removeMessages(0);
			sendMessageDelayed(obtainMessage(0), delayMillis);
		}
	};

	private void addBubbleLeft() {

		Paint white = new Paint();
		white.setColor(Color.WHITE);
		white.setAlpha(50);

		if (Math.random() * 100 * powerLeft > 25) {
			int rad = (int) (Math.random() * 10) + 3;
			Circle c = new Circle(50, getHeight(), rad, white);
			listBubbleLeft.add(c);

			Sounds.playSoundBubble(getContext());
		}

	}

	private void addBubbleRight() {

		Paint white = new Paint();
		white.setColor(Color.WHITE);
		white.setAlpha(50);

		if (Math.random() * 100 * powerRight > 25)

		{
			int rad = (int) (Math.random() * 10) + 3;
			Circle c = new Circle(getWidth() - 50, getHeight(), rad, white);
			listBubbleRight.add(c);

			Sounds.playSoundBubble(getContext());
		}

	}

	private void incBubbleLeft() {
		for (int i = 0; i < listBubbleLeft.size(); i++) {

			Circle c = listBubbleLeft.get(i);
			int random = (int) (Math.random() * 10);

			if (c.getY() < 50) {
				listBubbleLeft.remove(i);
			} else {
				c.move(random, -c.getY() / 10);
			}
		}
	}

	private void incBubbleRight() {
		for (int i = 0; i < listBubbleRight.size(); i++) {

			Circle c = listBubbleRight.get(i);
			int random = (int) (Math.random() * 10);

			if (c.getY() < 50) {
				listBubbleRight.remove(i);
			} else {
				c.move(-(random), -c.getY() / 10);
			}
		}
	}



	private void moveUpRings() {

		if (pressedLeft) {
			Log.i("presionados", "pressedLeft");

			addBubbleLeft();
			for (int i = 0; i < maxRings; i++) {
				Sprite s = arrayRings[i];

				if (this.stickLeft.estaColisionado(s)
						|| this.stickRight.estaColisionado(s)) {
					if (this.fit(s, stickLeft) || this.fit(s, stickRight)) {
						s.move(0, -(int) (Math.random() * 7 * powerLeft));

					}
				} else {
					int randomX = (int) ((Math.random() * 5) + 3);
					float getx = s.getX();
					float gety = s.getY();
					float randomy = (((getWidth() - getx) / 10) * (gety/3) * powerLeft) / 150;
					// Log.i("randomleft ",""+randomy);
					s.move(randomX, -randomy);
				}

			}

		}
		if (pressedRight) {
			Log.i("presionados", "pressedRight");
			addBubbleRight();
			for (int i = 0; i < maxRings; i++) {
				Sprite s = arrayRings[i];

				if (this.stickLeft.estaColisionado(s)
						|| this.stickRight.estaColisionado(s)) {
					if (this.fit(s, stickLeft) || this.fit(s, stickRight)) {
						s.move(0, -(int) (Math.random() * 7 * powerRight));

					}
				} else {
					int randomX = (int) ((Math.random() * 5) - 7);
					float getx = s.getX();
					float gety = s.getY();
					float randomy = ((getx / 10) * (gety/3) * powerRight) / 150;
					s.move(randomX, -randomy);
				}

			}

		}
	}

	private void moveDownRings() {
		// movimiento de caida y corrección de posiciones
		for (int i = 0; i < maxRings; i++) {
			Sprite s = arrayRings[i];

			if (this.stickLeft.estaColisionado(s)) {
				if (this.fit(s, stickLeft)) {
					if (isOver(s, stickLeft)) {
						s.move(0, (int) (Math.random() * 5));

					}

				} else {
					int posX = 5;
					if (s.getCenterX() < stickLeft.getCenterX())
						posX = -5;
					int randomY = (int) (Math.random() * 15);
					s.move(posX, randomY);
				}

			} else if (this.stickRight.estaColisionado(s)) {
				if (this.fit(s, stickRight)) {
					if (isOver(s, stickRight)) {
						s.move(0, (int) (Math.random() * 5));

					}

				} else {
					int posX = 5;
					if (s.getCenterX() < stickRight.getCenterX())
						posX = -5;
					int randomY = (int) (Math.random() * 15);
					s.move(posX, randomY);
				}
			} else if (s.getY() + s.getHeight() + 15 < getHeight()) {

				int randomX = (int) (Math.random() * 10 - 5);
				int randomY = (int) (Math.random() * getHeight() / 25);
				if (!(pressedLeft || pressedRight))
					s.move(randomX, randomY);

				s.rotateBitmap(randomX * 5);
			}

			if (s.getPosY() < 0) {
				s.setPosY(0);
			}
			if (s.getPosX() < 0) {
				s.setPosX(0);
			}
			if (s.getPosX() + s.getWidth() > getWidth()) {
				s.setPosX(getWidth() - s.getWidth());
			}
		}
	}

	private void update() {

		moveUpRings();

		// incremento de las burbujas
		incBubbleLeft();
		incBubbleRight();

		if (limitedAir)
			decrementPower();
		// isComplete=isComplete();

		if (isComplete) {
			complete();
		} else {
			isComplete = isComplete();
			moveDownRings();
		}
	}

	
	private void decrementPower() {

		if (powerLeft >= 0) {
			powerLeft -= 0.1;

		} else {
			pressedLeft = false;
			powerLeft = 0;
		}
		if (powerRight >= 0) {
			powerRight -= 0.1;
		} else {
			pressedRight = false;
			powerRight = 0;
		}
		Log.i("powerAir", "left " + powerLeft + " right " + powerRight);
	}

	private void complete() {

		int moveUp = getHeight() / 25;
		stickLeft.move(0, -moveUp);
		stickRight.move(0, -moveUp);
		if (!stickUp) {
			int middleY = (int) ((getHeight() / 2) - (stickRight.getHeight() / 2));
			if (middleY > stickLeft.getY()) {
				initDraw();
			}

		}
		for (int i = 0; i < maxRings; i++) {
			Sprite s = arrayRings[i];
			s.move(0, -moveUp);
		}
		if (stickLeft.getBottom() < 0) {
			stickLeft.setPosY(getHeight());
			stickRight.setPosY(getHeight());

			stickUp = false;
		}
	}

	private boolean isComplete() {
		boolean isFit = true;
		int i = 0;
		while (isFit && i < maxRings) {
			Sprite s = arrayRings[i];
			isFit = fit(s, stickLeft) || fit(s, stickRight);

			i++;
		}
		return isFit;
	}



	protected void initDraw() {

		limitedAir = Prefs.isLimitedAir(getContext());

		maxRings = Prefs.getNumberRingsValue(getContext());

		Paint white = new Paint();
		white.setColor(Color.WHITE);
		white.setAlpha(50);

		Bitmap bm = BitmapFactory.decodeResource(getResources(),
				R.drawable.background);
		// Bitmap bm2 = Bitmap.createScaledBitmap(bm,
		// getWidth(),getHeight(),true);//dstWidth, dstHeight, filter)
		background = new Sprite(bm, 0, 0, null);
		background.scaleBitmap(getWidth(), getHeight());
		// background = new Sprite(
		// BitmapFactory.decodeResource(getResources(),R.drawable.icon),0,0,null);

		circleLeft =  new Circle(getWidth()/5  , 6*getHeight() /7, getWidth()/10, white);
		circleRight = new Circle(4*getWidth()/5  , 6*getHeight() /7, getWidth()/10, white);

		pathLeft = new Path();
		pathLeft.addCircle(50, getHeight() - 50, 30, Direction.CW);
		pathRight = new Path();
		pathRight
				.addCircle(getWidth() - 50, getHeight() - 50, 30, Direction.CW);

		listBubbleLeft = new ArrayList<Circle>();
		listBubbleRight = new ArrayList<Circle>();
		arrayRings = new Sprite[maxRings];

		for (int i = 0; i < maxRings; i++) {
			int intx = getWidth() / maxRings;
			int random = (int) (Math.random() * 4);
			int imgRing = R.drawable.ring0;
			switch (random) {
			case 0:
				imgRing = R.drawable.ring0;
				break;
			case 1:
				imgRing = R.drawable.ring1;
				break;
			case 2:
				imgRing = R.drawable.ring2;
				break;
			case 3:
				imgRing = R.drawable.ring3;
				break;
			}
			Bitmap bubble = BitmapFactory.decodeResource(getResources(),
					imgRing);
			arrayRings[i] = new Sprite(bubble, intx * i, 0, null);
		}

		Matrix matrix = new Matrix();
		matrix.preScale(-1, 1);

		Bitmap stick = BitmapFactory.decodeResource(getResources(),
				R.drawable.stick);
		stickLeft = new Sprite(stick,
				(getWidth() / 3) - (stick.getWidth() / 2), (getHeight() / 2)
						- (stick.getHeight() / 2), null);

		stick = Bitmap.createBitmap(stick, 0, 0, stick.getWidth(),
				stick.getHeight(), matrix, false);

		stickRight = new Sprite(stick, (2 * getWidth() / 3)
				- (stick.getWidth() / 2), (getHeight() / 2)
				- (stick.getHeight() / 2), null);
		isComplete = false;
		stickUp = true;
	}

	public GameView(Context context) {
		super(context);
		// this.game = (Game) context;
		setFocusable(true);
		setFocusableInTouchMode(true);

	}

	protected void onDraw(Canvas canvas) {

		if (circleLeft == null) {
			initDraw();
			// Log.d("draw","draw");
		}
		redraw.sleep(100);

		background.draw(canvas);
		circleLeft.draw(canvas);
		circleRight.draw(canvas);

		stickLeft.draw(canvas);
		stickRight.draw(canvas);
		for (int i = 0; i < maxRings; i++) {
			arrayRings[i].draw(canvas);
		}

		for (int i = 0; i < listBubbleLeft.size(); i++) {
			listBubbleLeft.get(i).draw(canvas);
		}
		for (int i = 0; i < listBubbleRight.size(); i++) {
			listBubbleRight.get(i).draw(canvas);
		}

		Paint white = new Paint();
		white.setColor(Color.WHITE);

	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		dumpEvent(event);

		int action = event.getAction() & MotionEvent.ACTION_MASK;

		int pointer = 0;
		// isComplete = true;
		switch (action) {
		case MotionEvent.ACTION_POINTER_DOWN:
			pointer = 1;
		case MotionEvent.ACTION_DOWN:

			if (circleLeft.isClicked(event, pointer)) {
				pressedLeft = true;
				powerLeft = 1;
				if (Prefs.isSoundActive(getContext()))
					Sounds.playSoundAirPump(getContext());// .start();
			}
			if (circleRight.isClicked(event, pointer)) {
				pressedRight = true;
				powerRight = 1;
				if (Prefs.isSoundActive(getContext()))
					Sounds.playSoundAirPump(getContext());
			}

			break;
		case MotionEvent.ACTION_MOVE:

			break;

		case MotionEvent.ACTION_POINTER_UP:

			pointer = 1;
			if (event.getX(pointer) < getWidth() / 2) {
				pressedLeft = false;
			}
			if (event.getX(pointer) > getWidth() / 2) {
				pressedRight = false;
			}
			break;

		case MotionEvent.ACTION_UP:

			pressedLeft = false;
			pressedRight = false;

			break;
		}

		return true;
	}

	private void dumpEvent(MotionEvent event) {
		String names[] = { "DOWN", "UP", "MOVE", "CANCEL", "OUTSIDE",
				"POINTER_DOWN", "POINTER_UP", "7?", "8?", "9?" };
		StringBuilder sb = new StringBuilder();
		int action = event.getAction();
		int actionCode = action & MotionEvent.ACTION_MASK;
		sb.append("event ACTION_").append(names[actionCode]);
		if (actionCode == MotionEvent.ACTION_POINTER_DOWN
				|| actionCode == MotionEvent.ACTION_POINTER_UP) {
			sb.append("(pid ").append(
					action >> MotionEvent.ACTION_POINTER_ID_SHIFT);
			sb.append(")");
		}
		sb.append("[");
		for (int i = 0; i < event.getPointerCount(); i++) {
			sb.append("#").append(i);
			sb.append("(pid ").append(event.getPointerId(i));
			sb.append(")=").append((int) event.getX(i));
			sb.append(",").append((int) event.getY(i));
			if (i + 1 < event.getPointerCount())
				sb.append(";");
		}
		sb.append("]");
		Log.i("onTouchEvent", sb.toString() + " action " + action + " x "
				+ event.getX() + ", y " + event.getY());

	}

	private boolean isOver(Sprite s, Sprite stick) {

		return (s.getY() < stick.getY() + 10);

	}

	private boolean fit(Sprite s, Sprite stick) {

		boolean isOver = s.getY() < stick.getCenterY();
		float centerRing = s.getX() + (s.getWidth() / 2);
		float centerStick = stick.getX() + (stick.getWidth() / 2);
		int interval = 10;
		return (isOver && ((centerRing - interval) <= centerStick) && ((centerRing + interval) >= centerStick));

	}

}
