package net.coconauts.waterrings;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;


public class Sprite {
	Bitmap originalBitmap;
	Bitmap bitmap;
	float posX;
	float posY;
	Paint paint;
	
	public Sprite(Bitmap bitmap, float posX,float posY,Paint paint){
		this.originalBitmap = bitmap;
		this.bitmap = bitmap;
		this.posX = posX;
		this.posY = posY;
		this.paint = paint;
	}
	
	protected void draw(Canvas canvas){
		canvas.drawBitmap(bitmap, posX, posY, paint);
	}
	
	protected void scaleBitmap(int w, int h){
		bitmap = Bitmap.createScaledBitmap(bitmap, w,h,true);
	}
	protected void rotateBitmap(int degrees){
		Matrix matrix = new Matrix();
		matrix.postRotate(degrees);
      // matrix.postRotate(degrees);
		bitmap = originalBitmap.copy(bitmap.getConfig(), false);
		bitmap = Bitmap.createBitmap(bitmap, 0,0,bitmap.getWidth() ,bitmap.getHeight() ,matrix,true);
	}
	protected void move(float addX, float addY){
		
		posX +=addX;
		posY +=addY;
	}

	public Bitmap getBitmap() {
		return bitmap;
	}
	public void setBitmap(Bitmap bitmap) {
		this.bitmap = bitmap;
	}
	public float getPosX() {
		return posX;
	}
	public void setPosX(float posX) {
		this.posX = posX;
	}
	
	public float getPosY() {
		return posY;
	}
	public void setPosY(float posY) {
		this.posY = posY;
	}
	public Paint getPaint() {
		return paint;
	}
	public void setPaint(Paint paint) {
		this.paint = paint;
	}
	protected float getY(){
		return posY;
	}
	protected float getX(){
		return posX;
	}
	protected float getHeight(){
		return bitmap.getHeight();
	}
	protected float getWidth(){
		return bitmap.getWidth();
	}
	protected float getBottom(){
		return this.posY+this.getHeight();
	}
	protected float getRight(){
		return this.posX+this.getWidth();
	}
	protected float getCenterX(){
		return this.posX+this.getWidth()/2;
	}
	protected float getCenterY(){
		return this.posY+this.getHeight()/2;
	}
	
	private boolean estaEntre(float x1, float x2, float x){
		
		return ((x1 < x) && (x2 >x));
	}
	public boolean estaColisionado(Sprite s){
		
		boolean estaEntreX = 
			(this.estaEntre(this.getX(), this.getRight(), s.getX() )) ||
			(this.estaEntre(this.getX(), this.getRight(), s.getRight() ));
		boolean estaEntreY = 
			(this.estaEntre(this.getY(), this.getBottom(), s.getY() )) ||
			(this.estaEntre(this.getY(), this.getBottom(), s.getBottom() ));
		
		return estaEntreX && estaEntreY;
				
	}
  
}
