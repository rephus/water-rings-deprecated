package net.coconauts.waterrings;

import net.coconauts.waterrings.R;

import android.content.Context;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;


public class Prefs extends PreferenceActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.settings);
	}
	
	public static String getNumberRings(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context).getString("numberRings","10");//getBoolean("ballSpeed",true);
	}
	
	
	public static int getNumberRingsValue(Context context) {

		String nr = Prefs.getNumberRings(context);
		int nrValue = 10;
		if (nr.equalsIgnoreCase("A few")){
			nrValue = 5;
		}
		else if (nr.equalsIgnoreCase("Normal")){
			nrValue = 10;
		}
		else if (nr.equalsIgnoreCase("A lot")){
			nrValue = 20;
		}
	

		return nrValue;
	}
	public static boolean isSoundActive(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("sound", true);//getBoolean("ballSpeed",true);
	}
	public static boolean isLimitedAir(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("limitedAir", true);//getBoolean("ballSpeed",true);
	}
}