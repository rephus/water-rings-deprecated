package net.coconauts.waterrings;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class Game extends Activity {
	GameView gameView;

	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		gameView = new GameView(this);
		setContentView(gameView);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		super.onCreateOptionsMenu(menu);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.settings:
			startActivity(new Intent(this, Prefs.class));
			return true;

		case R.id.exit:
			finish();
			return true;
		}
		return false;
	}

	public boolean onKeyDown(int keyCode, KeyEvent msg) {

		switch (keyCode) {

		case KeyEvent.KEYCODE_BACK:
			finish();
			break;
		}
		return false;
	}

}
