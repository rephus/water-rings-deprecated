package net.coconauts.waterrings;

//import com.google.ads.*;

import net.coconauts.waterrings.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;

public class Main extends Activity implements OnClickListener {
	/** Called when the activity is first created. */
	//private AdView adView;
	MainView mainView;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	//	LinearLayout layout = (LinearLayout) findViewById(R.id.mainLayout);
		LayoutParams lp =new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	
		setContentView(R.layout.main);
		mainView= new MainView(this);

		addContentView(mainView,lp);
	

		View vPlay = findViewById(R.id.btplay);
		vPlay.setOnClickListener(this);
		View vOptions = findViewById(R.id.btoptions);
		vOptions.setOnClickListener(this);
		View vAbout = findViewById(R.id.btabout);
		vAbout.setOnClickListener(this);
		View vExit = findViewById(R.id.btexit);
		vExit.setOnClickListener(this);

		//adView = new AdView(this, AdSize.BANNER, "a14f25a4541f76b");

		/*View vExit = findViewById(R.id.btexit);
		*/
		// Add the adView to it
		//layout.addView(adView);

		// Initiate a generic request to load it with an ad
		//adView.loadAd(new AdRequest());

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {

		case R.id.btplay:
			startActivity(new Intent(this, Game.class));
			break;
		case R.id.btoptions:
			startActivity(new Intent(this, Prefs.class));
			break;

		case R.id.btabout:
			startActivity(new Intent(this, About.class));

			break;
		case R.id.btexit:
			finish();
			break;

		}
	}

	  @Override
	  public void onDestroy() {
	  //  adView.destroy();
	    super.onDestroy();
	  }
}