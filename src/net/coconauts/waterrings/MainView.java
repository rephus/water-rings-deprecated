package net.coconauts.waterrings;

import java.util.ArrayList;
import java.util.List;

import net.coconauts.waterrings.Circle;
import net.coconauts.waterrings.R;

import android.content.Context;
import android.graphics.*;
import android.graphics.Path.Direction;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Message;

import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class MainView extends View {

	private RefreshHandler redraw = new RefreshHandler();
	private float powerLeft = 0;
	private float powerRight = 0;
	private Paint white = new Paint();
	private List<Circle> listBubbleLeft;
	private List<Circle> listBubbleRight;

	class RefreshHandler extends Handler {

		@Override
		public void handleMessage(Message msg) {
			// Log.d("RefreshHandler","handle");

			MainView.this.update();
			MainView.this.invalidate();
			// SnakeView.this.invalidate();
		}

		public void sleep(long delayMillis) {
			// Log.d("RefreshHandler","sleep");

			this.removeMessages(0);
			sendMessageDelayed(obtainMessage(0), delayMillis);
		}
	};

	private void addBubbleLeft() {

		if (Math.random() * 100 * powerLeft > 25)

		{
			int rad = (int) (Math.random() * 10) + 3;
			Circle c = new Circle(50, getHeight(), rad, white);
			listBubbleLeft.add(c);

			//playSoundBubble();
			Sounds.playSoundBubble(getContext());
		}

	}


	private void addBubbleRight() {

		if (Math.random() * 100 * powerRight > 25)

		{
			int rad = (int) (Math.random() * 10) + 3;
			Circle c = new Circle(getWidth() - 50, getHeight(), rad, white);
			listBubbleRight.add(c);

			Sounds.playSoundBubble(getContext());
		}

	}

	private void incBubbleLeft() {
		for (int i = 0; i < listBubbleLeft.size(); i++) {

			Circle c = listBubbleLeft.get(i);
			int random = (int) (Math.random() * 10);

			if (c.getY() < 50) {
				listBubbleLeft.remove(i);
			} else {
				c.move(random, -c.getY() / 10);
			}
		}
	}

	private void incBubbleRight() {
		for (int i = 0; i < listBubbleRight.size(); i++) {

			Circle c = listBubbleRight.get(i);
			int random = (int) (Math.random() * 10);

			if (c.getY() < 50) {
				listBubbleRight.remove(i);
			} else {
				c.move(-(random), -c.getY() / 10);
			}
		}
	}



	private void update() {
		powerLeft = 1;
		powerRight = 1;

		// incremento de las burbujas
		incBubbleLeft();
		incBubbleRight();

		addRandomBubble();
	}

	private void addRandomBubble() {

		int r = (int) (Math.random() * 100);
		Log.d("bubble", "r " + r);
		if (r > 85) {
			Log.d("bubble", "right");
			addBubbleRight();
		} else if (r < 15) {
			Log.d("bubble", "left");
			addBubbleLeft();
		}
	}

	

	private void initSound() {

		Sounds.initSound(getContext());
		
	}


	protected void initDraw() {

		white.setColor(Color.WHITE);
		white.setAlpha(50);
		
		listBubbleLeft = new ArrayList<Circle>();
		listBubbleRight = new ArrayList<Circle>();

	}

	public MainView(Context context) {
		super(context);

	}

	protected void onDraw(Canvas canvas) {

		if (listBubbleLeft == null) {
			initDraw();
			initSound();
			// Log.d("draw","draw");
		}
		redraw.sleep(100);

		for (int i = 0; i < listBubbleLeft.size(); i++) {
			listBubbleLeft.get(i).draw(canvas);
		}
		Log.d("bubble", "bubble right size " + listBubbleRight.size());
		for (int i = 0; i < listBubbleRight.size(); i++) {

			listBubbleRight.get(i).draw(canvas);
		}

	}
	
}
