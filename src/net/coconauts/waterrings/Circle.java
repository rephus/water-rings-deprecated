package net.coconauts.waterrings;

import android.annotation.SuppressLint;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.MotionEvent;

public class Circle {
	private  float posX;
	private float posY;
	private float radious;
	private Paint paint;
	
	Circle(float posX,float posY,float radious,Paint paint){
		
		this.posX = posX;
		this.posY = posY;
		this.radious = radious;
		this.paint = paint;
	}
	
	
	protected void draw(Canvas canvas){
		canvas.drawCircle(posX,posY,radious,paint);
	}
	@SuppressLint("NewApi")
	protected boolean isClicked(MotionEvent event,int pointer){
		float difx = this.posX-event.getX(pointer);
		double doublex = Math.pow(difx, 2) ;
		float dify = this.posY-event.getY(pointer);
		double doubley = Math.pow(dify, 2) ;
		
		double distance = Math.sqrt(doublex+ doubley);
		
		return distance < radious;
	}

protected void move(float addX, float addY){
		
		posX +=addX;
		posY +=addY;
	}
	protected float getY(){
		return posY;
	}
	protected float getX(){
		return posX;
	}
}
